use std::sync::Arc;
use vulkano::{
	pipeline::GraphicsPipeline, 
	descriptor::PipelineLayoutAbstract, 
	framebuffer::RenderPassAbstract};
use crate::common::Vertex;

pub mod offloaded_texture;
pub mod mesh;
pub mod mesh_instance;

type Pipeline = Arc<GraphicsPipeline<vulkano::pipeline::vertex::SingleBufferDefinition<Vertex>, Box<dyn PipelineLayoutAbstract + Send + Sync>, Arc<dyn RenderPassAbstract + Send + Sync>>>;
