// // note that this is defined in column order, but displayed as if in row order because that's how code works
// pub const PROJECTION_CORRECTION_MATRIX: Matrix4<f32> = Matrix4::from_cols(
// 	Vector4::new(1.0, 0.0, 0.0, 0.0), 
// 	Vector4::new(0.0, -1.0, 0.0, 0.0), 
// 	Vector4::new(0.0, 0.0, 0.5, 0.0), 
// 	Vector4::new(0.0, 0.0, 0.5, 1.0));

#[allow(dead_code)]
pub mod math_types {
	pub type Vec4 = [f32; 4];
	pub type Vec3 = [f32; 3];
	pub type Vec2 = [f32; 2];

	pub type Matrix3 = cgmath::Matrix3<f32>;
	pub type Matrix4 = cgmath::Matrix4<f32>;

	pub type Vector2 = cgmath::Vector2<f32>;
	pub type Vector3 = cgmath::Vector3<f32>;
	pub type Vector4 = cgmath::Vector4<f32>;

	pub type Rad = cgmath::Rad<f32>;
	// pub type Euler = cgmath::Euler<f32>;
	pub type Quaternion = cgmath::Quaternion<f32>;
	pub type Point3 = cgmath::Point3<f32>;
}
use math_types::*;

#[allow(dead_code)]
pub mod vec_math;
pub mod math;
use vec_math::*;



#[derive(Default, Copy, Clone)]
pub struct Vertex {
	pub position: Vec3,
	pub uv: Vec2,
	pub normal: Vec3,
	pub tangent: Vec3,
	pub bitangent: Vec3
}
impl std::cmp::PartialEq for Vertex {
	// tangent and bitangent are ignored because they are merely properties for equivalent vertices to share
	fn eq(&self, other: &Self) -> bool {
		for i in 0..3 {
			if self.position[i] != other.position[i] {
				return false;
			}
			if self.normal[i] != other.normal[i] {
				return false;
			}
		}
		for i in 0..2 {
			if self.uv[i] != other.uv[i] {
				return false;
			}
		}
		return true;
	}
	
}


impl Vertex {
	pub fn new(position: Vec3, uv: Option<Vec2>, normal: Option<Vec3>, tan: Option<Vec3>, bitan: Option<Vec3>) -> Vertex {
		Vertex {
			position,
			uv: uv.unwrap_or([0.0_f32, 0.0]),
			normal: normal.unwrap_or([0.0_f32, 1.0, 0.0]),
			tangent: tan.unwrap_or([1.0_f32, 0.0, 0.0]),
			bitangent: bitan.unwrap_or([0.0_f32, 0.0, 1.0])
		}
	}
	pub fn join(self, other: Vertex) -> Vertex {
		let mut ret = self;
		ret.tangent = add_vec3(ret.tangent, div_vec3_scalar(sub_vec3(self.tangent, other.tangent), 2.0));
		ret.bitangent = add_vec3(ret.bitangent, div_vec3_scalar(sub_vec3(self.bitangent, other.bitangent), 2.0));
		ret 
	}
}

vulkano::impl_vertex!(Vertex, position, uv, normal, tangent, bitangent);

pub fn get_transformation_matrix(scale: Vector3, translation: Vector3, rotation: Quaternion) -> Matrix4 {
	let scale_mat = Matrix4::from_nonuniform_scale(scale.x, scale.y, scale.z);
	let trans_mat = Matrix4::from_translation(translation);
	let rot_mat = Matrix4::from(rotation);
	trans_mat * rot_mat * scale_mat
}

pub fn extract_topleft_matrix(matrix: &Matrix4) -> Matrix3 {
	Matrix3::new(
		matrix.x[0], matrix.x[1], matrix.x[2], 
		matrix.y[0], matrix.y[1], matrix.y[2], 
		matrix.z[0], matrix.z[1], matrix.z[2], 
	)
}

pub fn rad(n: f32) -> Rad {
	cgmath::Rad(n)
}

pub fn get_projection_matrix(fov: Rad, aspect_ratio: f32, near: f32, far: f32) -> Matrix4 {
	cgmath::Matrix4::from_nonuniform_scale(1.0, -1.0, 1.0) * cgmath::perspective(
		fov,
		aspect_ratio,
		near,
		far
  )
}

// pub fn matrix_to_array(mat: Matrix4) -> [[f32; 4]; 4] {
// 	[
// 		[mat.x[0], mat.x[1], mat.x[2], mat.x[3]],
// 		[mat.y[0], mat.y[1], mat.y[2], mat.y[3]],
// 		[mat.z[0], mat.z[1], mat.z[2], mat.z[3]],
// 		[mat.w[0], mat.w[1], mat.w[2], mat.w[3]],
// 	]
// }

pub trait VectorMath {
	fn normalized(&self) -> Self;
	fn length(&self) -> f32;
	fn dot(&self, other: Self) -> f32;
}

impl VectorMath for Vector3 {
	fn normalized(&self) -> Vector3 {
		let len = self.length();
		let t = Vector3::new(self.x / len, self.y / len, self.z / len);
		t
	}

	fn length(&self) -> f32 {
		(self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
	}
	fn dot(&self, other: Self) -> f32 {
		(self.x * other.x) + (self.y * other.y) + (self.z * other.z)
	}
}