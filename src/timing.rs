
use game_time::{GameClock, step::VariableStep, framerate::RunningAverageSampler, FrameCounter, FrameCount, FrameRateSampler};

pub struct Timing {
	time_step: VariableStep,
	game_clock: GameClock,
	sampler: RunningAverageSampler,
	frame_counter: FrameCounter<RunningAverageSampler>,
	last_print_time: i64,
	is_print_tick: bool,
}
impl Timing {
	pub fn new() -> Self {
		let sampler = RunningAverageSampler::with_max_samples(30);
		let frame_counter = FrameCounter::new(120.0, sampler.clone());
		Timing {
			time_step: VariableStep::new(),
			game_clock: GameClock::new(),
			sampler,
			frame_counter,
			last_print_time: 0,
			is_print_tick: false,
		}
	}

	pub fn is_print_tick(&self) -> bool {
		self.is_print_tick
	}

	pub fn step_timing(&mut self) -> f64 {
		self.is_print_tick = false;
		// this is done before ticking the clock to get the total elapsed time of the *previous* frame
		let delta = self.game_clock.frame_elapsed_time().as_seconds(); //(time_passed) - last_frame_start_time;
		let time = self.game_clock.tick(&self.time_step); 
		self.frame_counter.tick(&time);
		self.sampler.tick(&time);

		let time_passed = self.game_clock.frame_start_time().timestamp();
		if time_passed > self.last_print_time + 1 {
			self.is_print_tick = true;
			// frame_counter *should* handle this but for some reason it thinks my framerate is infinite soooo
			// we're using a sampler directly
			println!("{:.2} FPS", self.sampler.average_frame_rate());
			self.last_print_time = time_passed;
			// frame_count = 0;

		}
		// frame_count += 1;	
		delta
	}
	pub fn sleep_remaining(&mut self) {
		self.game_clock.sleep_remaining(&self.frame_counter);
	}
}
