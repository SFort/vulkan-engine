use vulkano::instance::Instance;
use vulkano::{device::{Queue, Device}, framebuffer::RenderPassAbstract};
use std::{sync::Arc, rc::Rc};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::{dpi::{LogicalSize, Size}, event::{self, Event}};
use rand::{SeedableRng, prelude::StdRng};
use event::{VirtualKeyCode, WindowEvent, ElementState};

mod file_loading;
mod common;
mod offloaded_assets;
mod timing;
mod rendering;
mod input_map;

use offloaded_assets::mesh_instance::{PhysicsModel, MeshInstance};
use timing::Timing;
use rendering::render_server::RenderServer;
use shaders::vs;
use input_map::{KeyId, InputMap};
use common::math_types::Vector3;

extern crate shaders;

static MIN_INNER_SIZE: Size = Size::Logical(LogicalSize::new(256.0, 256.0));
const MOVE_SPEED: f32 = 0.5;

fn get_objects(device: Arc<Device>, queue: Arc<Queue>, render_pass: Arc<dyn RenderPassAbstract + Send + Sync>) -> Vec<Rc<dyn MeshInstance>> {
	let mut rng_gen: StdRng = StdRng::from_entropy();
	let mut ret: Vec<Rc<dyn MeshInstance>> = Vec::new();
	ret.push(Rc::new(PhysicsModel::new(
		String::from("assets/models/newell_teaset/teapot_upgrade.obj"), 
		device.clone(), 
		queue.clone(), 
		render_pass.clone(),
		Vector3::new(-2.5, -1.0, 0.0),
		&mut rng_gen
	)));

	println!("Setting up second teapot");

	ret.push(Rc::new(PhysicsModel::new(
		String::from("assets/models/newell_teaset/teapot_upgrade.obj"), 
		device.clone(), 
		queue.clone(), 
		render_pass.clone(),
		Vector3::new(2.5, -1.0, 0.0),
		&mut rng_gen
	)));
	
	ret
}

//TODO: I doubt this is the idiosyncratic way to do this
static mut INSTANCE: Option<Arc<Instance>> = None;

fn main() {

	unsafe {
		INSTANCE = Some({
			let extensions = vulkano_win::required_extensions();
			Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
		});
	}
	let events_loop = EventLoop::new();
	let mut input_map = InputMap::new();
	let mut render_server = unsafe {
		RenderServer::new(INSTANCE.as_ref().unwrap(), &events_loop)
	};

	input_map.create_action("toggle_antialias".into(), vec![KeyId::VirtualId(VirtualKeyCode::Return)]);
	input_map.create_action("quit".into(), vec![KeyId::VirtualId(VirtualKeyCode::Escape)]);

	input_map.create_action("move_forward".into(), vec![KeyId::VirtualId(VirtualKeyCode::W)]);
	input_map.create_action("move_backward".into(), vec![KeyId::VirtualId(VirtualKeyCode::S)]);
	input_map.create_action("move_left".into(), vec![KeyId::VirtualId(VirtualKeyCode::A)]);
	input_map.create_action("move_right".into(), vec![KeyId::VirtualId(VirtualKeyCode::D)]);
	input_map.create_action("move_up".into(), vec![KeyId::VirtualId(VirtualKeyCode::Space)]);
	input_map.create_action("move_down".into(), vec![KeyId::VirtualId(VirtualKeyCode::LControl)]);
	render_server.set_window_size(1000.0, 800.0);
	render_server.add_objects(
		get_objects(
			render_server.device.clone(), 
			render_server.queue.clone(), 
			render_server.render_pass.clone()
		)
	);
	render_server.add_light(vs::ty::light {
		_dummy0: [0u8; 4],
		color: [1.0; 3],
		position: [0.0, 2.0, 2.0],
		strength: 2.0
	}).expect("Failed to add light");

	let mut timing = Timing::new();

	let mut antialiasing = true;
	// timing.step_timing();
	
	let mut player_motion = Vector3::new(0.0, 0.0, 0.0);
	println!("Entering event loop!");
	events_loop.run(move |event, _, control_flow| {
		match event {
			Event::WindowEvent { event: event::WindowEvent::CloseRequested, .. } => {
				*control_flow = ControlFlow::Exit;
			},
			Event::WindowEvent { event: WindowEvent::Resized(_), .. } => {
				render_server.trigger_recreate_swapchain();
			},
			Event::WindowEvent { event: WindowEvent::KeyboardInput {input, ..}, .. } => {
				input_map.set_key_pressed(KeyId::ScancodeId(input.scancode), input.state == ElementState::Pressed);
				if let Some(keycode) = input.virtual_keycode {
					input_map.set_key_pressed(KeyId::VirtualId(keycode), input.state == ElementState::Pressed);
				}
			},
			event::Event::RedrawEventsCleared => {},
			event::Event::MainEventsCleared => {
				// we do this at the start of the loop instead of the end to maintain a consistent framerate regardless of the other events we need to process
				// (like keyboard input)
				timing.sleep_remaining();
				let delta = timing.step_timing();
				// air resistance, essentially
				player_motion *= 0.96;

				if input_map.is_action_just_pressed("toggle_antialias".into()) {
					render_server.set_msaa_level(if antialiasing {1} else {8}).expect("failed to change antialiasing mode");
					antialiasing = !antialiasing;
				}
				if input_map.is_action_just_pressed("quit".into()) {
					*control_flow = ControlFlow::Exit;
				}

				if input_map.is_action_pressed("move_forward".into()) {
					player_motion.z = player_motion.z - MOVE_SPEED;
				}
				if input_map.is_action_pressed("move_backward".into()) {
					player_motion.z = player_motion.z + MOVE_SPEED;
				}
				if input_map.is_action_pressed("move_left".into()) {
					player_motion.x = player_motion.x - MOVE_SPEED;
				}
				if input_map.is_action_pressed("move_right".into()) {
					player_motion.x = player_motion.x + MOVE_SPEED;
				}
				if input_map.is_action_pressed("move_down".into()) {
					player_motion.y = player_motion.y - MOVE_SPEED;
				}
				if input_map.is_action_pressed("move_up".into()) {
					player_motion.y = player_motion.y + MOVE_SPEED;
				}
				render_server.camera_position += player_motion * (delta as f32);
				if timing.is_print_tick() {
					println!("Camera is at {:?}", render_server.camera_position);
				}
				render_server.draw(delta);
				input_map.clear_recently_changed();
			},
			_ => {}
		}
  });
}
