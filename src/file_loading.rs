use crate::common::{math_types::{Vector2, Vector3}, Vertex, VectorMath};
use obj::{ObjData, IndexTuple};
use image::{ColorType, ImageDecoder};
use std::{fs::File, io::BufReader};
use regex::Regex;
use vulkano::format::Format;

static mut FILENAME_R: Option<Regex> = None;

// helper function
fn arr_to_vec(pos: [f32; 3]) -> Vector3 {
	Vector3::from([pos[0], pos[1], pos[2]])
}


fn checked_add_vert(data: &mut Vec<Vertex>, index_array: &mut Vec<u32>, obj_data: &ObjData, tri_vertices: [&IndexTuple; 3]) {
	// we're loading the vertex data from the model and generating tangent/bitangent data here
	// vertex position shortcuts
	let v0: Vector3 = arr_to_vec(obj_data.position[tri_vertices[0].0]);
	let v1: Vector3 = arr_to_vec(obj_data.position[tri_vertices[1].0]);
	let v2: Vector3 = arr_to_vec(obj_data.position[tri_vertices[2].0]);
	// uv shortcuts
	
	let uv0 = Vector2::from(match tri_vertices[0].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	});
	let uv1 = Vector2::from(match tri_vertices[1].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	});
	let uv2 = Vector2::from(match tri_vertices[2].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	});

	let delta_pos_1: Vector3 = v1 - v0;
	let delta_pos_2: Vector3 =  v2 - v0;

	let delta_uv_1: Vector2 = uv1 - uv0;
	let delta_uv_2: Vector2 = uv2 - uv0;
	let r = 1.0_f32 / (delta_uv_1[0] * delta_uv_2[1] - delta_uv_1[1] * delta_uv_2[0]);
	let tan: Vector3 = (delta_pos_1 * delta_uv_2[1]) - (delta_pos_2 * delta_uv_1[1]) * r;

	let bitan: Vector3 = ((&delta_pos_2 * delta_uv_1[0] - &delta_pos_1 * delta_uv_2[0]) * r).normalized();

	let mut normal = arr_to_vec(match tri_vertices[0].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0]
	});
	let mut first_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		first_tan = first_tan * -1.0;
	}
	let first_vert = Vertex::new(
		v0.into(),
		Some(uv0.into()),
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		Some(normal.into()),
		Some(first_tan.into()),
		Some(bitan.into())
	);
	normal = arr_to_vec(match tri_vertices[1].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0]
	});
	let mut second_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		second_tan = second_tan * -1.0;
	}
	let second_vert =Vertex::new(
		v1.into(),
		Some(uv1.into()),
		Some(normal.into()),
		Some(second_tan.into()),
		Some(bitan.into())
	);
	normal = arr_to_vec(match tri_vertices[2].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0]
	});
	let mut third_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		third_tan = third_tan * -1.0;
	}
	let third_vert =Vertex::new(
		v2.into(),
		Some(uv2.into()),
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		Some(normal.into()),
		Some(third_tan.into()),
		Some(bitan.into())
	);
	check_and_add_vert(data, index_array, first_vert);
	check_and_add_vert(data, index_array, second_vert);
	check_and_add_vert(data, index_array, third_vert);
}

fn check_and_add_vert(data: &mut Vec<Vertex>, index_array: &mut Vec<u32>, mut current_vert: Vertex) {
	// I doubt this is the most efficient way to do this, but from everything I tried this was the only way I got to work. TODO: try making this better
	// Using rposition instead of position because most repeat vertices will most likely be recently added
	// For reference - loading the Utah Teapot takes ~1.87 seconds using `position` and ~0.45 seconds using `rposition`
	let vert_index = match data.iter().rposition(|&v| v == current_vert) {
		Some(v) => v as i64,
		None => -1
	};
	if vert_index != -1 {
		// combine the tangent and bitangent values
		let target_vert = data.get(vert_index as usize).expect("failed to read vertex");
		current_vert = current_vert.join(*target_vert);
		data[vert_index as usize] = current_vert;
		index_array.push(vert_index as u32); // using -1 as the default index because otherwise there'd be confusion about what is and isn't a valid point - 
		// is Integer.MAX_VALUE the index of the vertex, or the value indicating no data?
	} else {
		data.push(current_vert);
		index_array.push((data.len() - 1) as u32);
	}
}

// note that this code is still RIDDLED with `expect`s, `panic!`s, and `assert`s - it's VERY likely to crash the program if ANYTHING goes wrong at ALL
pub fn load_ogl_model(path: &str, data: &mut Vec<Vertex>, index_array: &mut Vec<u32>) {
	let test_object_result = obj::Obj::load(path);
	let test_object = match test_object_result {
		Ok(obj) => obj,
		Err(e) => panic!(e)
	};
	// if `data` has a non-multiple of 3 amount of vertices in it, trying to add a triangle will instead create a mishapen triangle using the vertices already in the buffer
	assert_eq!(data.len() % 3, 0);
	// for now, I'm only working under the assumption that there is EXACTLY ONE object in the model file.
	// eventually I'd like to expand this and make the conversion more robust
	assert_eq!(test_object.data.objects.len(), 1);

	let obj_object = match test_object.data.objects.get(0) {
		Some(obj) => obj,
		None => panic!("Failed to load obj internal object")
	};

	for obj_group in obj_object.groups.iter() {

		data.reserve(obj_group.polys.len() * 3);

		for poly in obj_group.polys.iter() {

			// using simple triangle fan triangulation - We're assuming all polygons are complex because FUCK this shit gets complicated
			for i in 2..poly.0.len() {
				let tri_start_vert = poly.0.get(0).expect("indexing error");
				let tri_mid_vert = poly.0.get(i - 1).expect("indexing error");
				let tri_last_vert = poly.0.get(i).expect("indexing error");
				checked_add_vert(data, index_array, &test_object.data, [tri_start_vert, tri_mid_vert, tri_last_vert]);
			}
		}
	}

}

/// Converts `ColorType`s to their appropriate matching `Format`.
///
/// Note: the returned `Format` is the closest matching universally-accepted format, not necessarily the exact same bits-per-pixel.
/// Namely, any RGB/BGR format without an alpha channel will need to have one zippered on.
/// # Panics
/// This method panics if it doesn't recognize the ColorType.
fn convert_ctype_to_format(ctype: ColorType, prefer_srgb: bool) -> Format {
	match ctype {
	    ColorType::L8 =>     Format::R8Unorm,
		 ColorType::La8 =>    Format::R8G8Unorm,
	    ColorType::Rgb8 =>   if prefer_srgb {Format::R8G8B8A8Srgb} else {Format::R8G8B8A8Unorm},
	    ColorType::Rgba8 =>  if prefer_srgb {Format::R8G8B8A8Srgb} else {Format::R8G8B8A8Unorm},
	    ColorType::L16 =>    Format::R16Sfloat,
	    ColorType::La16 =>   Format::R16G16Sfloat,
	    ColorType::Rgb16 =>  Format::R16G16B16A16Sfloat,
	    ColorType::Rgba16 => Format::R16G16B16A16Sfloat,
	    ColorType::Bgr8 =>   if prefer_srgb {Format::B8G8R8A8Srgb} else {Format::B8G8R8A8Unorm},
	    ColorType::Bgra8 =>  if prefer_srgb {Format::B8G8R8A8Srgb} else {Format::B8G8R8A8Unorm},
	    ColorType::__NonExhaustive(_) => panic!("Unrecognized color type!")
	}
}


#[allow(dead_code)]
pub fn load_texture(path: &str, prefer_srgb: bool) -> (Vec<u8>, (u32, u32), Format) {
	let start_time = std::time::Instant::now();
	let dfile = std::fs::File::open(path).expect("failed to load data");
	// glium::Texture2d::new(facade, data).expect("Failed to generate texture")
	let (mut data, color_type, dimensions) = 
		if path.ends_with(".png") {
			load_texture_png(dfile)
		} else if path.ends_with(".jpg") || path.ends_with(".jpeg") {
			load_texture_jpg(dfile)
		} else if path.ends_with(".ff") || path.ends_with(".farbfeld") { //.farbfeld is technically invalid but I bet some people use it
			load_texture_ff(dfile)
		} else {
			panic!("Invalid texture type!");
	};
	// It doesn't matter if it's BGR or RGB, if the bpp is 24 we need to add an alpha channel
	if color_type.bits_per_pixel() == 24 {
		data = add_alpha_channel(data);
	} else if color_type.bits_per_pixel() == 48 {
		data = add_alpha_channel_16(data);
	}
	// sigh... Rust, can you trust me just a LITTLE bit? static mut isn't THAT evil
	let r = 
		unsafe {
			match &FILENAME_R {
				Some(regex) => {
					regex
				},
				None => {
					FILENAME_R = Some(Regex::new(r"(.*/)*(.*?\..*?$)").expect("Failed to build regex"));
					FILENAME_R.as_ref().unwrap()
				}
			}
		};
	let filename_opt = r.captures(path);
	if let Some(filename_cap) = filename_opt {
		if let Some(filename) = filename_cap.get(2) {
			println!("[Image Decoder] Returning image \"{}\"; took {:.4} seconds", filename.as_str(), start_time.elapsed().as_secs_f64());
		} else {
			println!("[Image Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
		}
	} else {
		println!("[Image Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
	}
	(data, dimensions, convert_ctype_to_format(color_type, prefer_srgb))
}

pub fn load_texture_png(dfile: File) -> (Vec<u8>, ColorType, (u32, u32)) {
	let img = image::png::PngDecoder::new(dfile).expect("Failed to load image file");
	let color_type = img.color_type();
	let total_bytes = img.total_bytes() as usize;
	let mut data: Vec<u8> = vec![0u8; total_bytes];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	(data, color_type, dimensions)
}

#[allow(dead_code)]
pub fn load_texture_dds(path: &str) -> (Vec<Vec<u8>>, Vec<(u32, u32)>, u32) {
	let start_time = std::time::Instant::now();
	let dfile = std::fs::File::open(path).expect("failed to load data");
	let mut reader = BufReader::new(dfile);
	let dds = ddsfile::Dds::read(&mut reader).expect("Failed to read dds file");
	let width = dds.get_width() as usize;
	let height =  dds.get_height() as usize;
	let mut dimensions = Vec::new();

	// If the file is encoded using DXT1, it only uses 64 bits per 16 pixels; otherwise it uses 128 bits
	let is_64_bit = dds.get_d3d_format().expect("Unrecognized file format") == ddsfile::D3DFormat::DXT1;

	let mut data = Vec::with_capacity(dds.get_num_mipmap_levels() as usize);

	let temp_data = dds.get_data(0).expect("Failed to load data");

	println!("[DDS Decoder] Array layer count: {}", dds.get_num_array_layers());
	if dds.get_num_array_layers() > 1 {
		println!("[DDS Decoder] [WARNING] More than one array layer! This is unsupported!");
	}

	// 64 bits per 16 pixels = 0.5 bytes per pixel; this is not directly true, but it is the average
	let tex_size_bytes = width * height / if is_64_bit {2} else {1};
	let mut start_index = 0;
	println!("Texture is size {}x{}; Total byte count estimated as {}", dds.get_width(), dds.get_height(), tex_size_bytes);

	for mip_level in 0..dds.get_num_mipmap_levels() {
		let mip_width = width / 2usize.pow(mip_level);
		let mip_height = height / 2usize.pow(mip_level);
		let end_index = start_index + (mip_width * mip_height / if is_64_bit {2} else {1});

		data.push(Vec::from(&temp_data[start_index..end_index]));
		dimensions.push((mip_width as u32, mip_height as u32));
		start_index = end_index;
	}
	println!("Finished loading mipmaps into buffers; Final byte coordinate is {}, expected was {}", start_index, tex_size_bytes);
	if start_index != tex_size_bytes {
		println!("[DDS Decoder] WARNING: start_index does not match tex_size_bytes!");
	}
		
	let mipcount = dds.get_num_mipmap_levels();

	println!("[DDS Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
	println!("[DDS Decoder] Image info: Dimensions: {:?}   Format: {:?}", [dimensions[0].0, dimensions[0].1], dds.get_d3d_format());

	(data, dimensions, mipcount)
}

// This function zippers an alpha channel onto 8-bpc textures, since GPUs almost never support 24-bit color.
fn add_alpha_channel(opaque_data: Vec<u8>) -> Vec<u8> {
	// this is ugly and probably very bad code but I'm tired so fuck it
	let total_bytes = opaque_data.len();
	let mut data: Vec<u8> = vec![255u8; total_bytes / 3 * 4];
	let mut bytes_offset = 0_usize;
	for i in 0..total_bytes {
		data[i + bytes_offset] = opaque_data[i];
		if i % 3 == 2 {
			bytes_offset = bytes_offset + 1;
		}
	}
	data	
}

// WARNING: THIS IS COMPLETELY UNTESTED. I don't have any 16 bpc textures to test with and I really can't be fucked to go find one.
// Use at your own risk.
// This function zippers an alpha channel onto 16-bpc textures, since GPUs almost never support 48-bit color.
fn add_alpha_channel_16(opaque_data: Vec<u8>) -> Vec<u8> {
	// this is ugly and probably very bad code but I'm tired so fuck it
	let total_bytes = opaque_data.len();
	let mut data: Vec<u8> = vec![255u8; total_bytes / 3 * 4];
	let mut bytes_offset = 0_usize;
	for i in 0..total_bytes {
		data[i + bytes_offset] = opaque_data[i];
		if i % 6 == 5 {
			bytes_offset = bytes_offset + 2;
		}
	}
	data
}


pub fn load_texture_jpg(dfile: File) -> (Vec<u8>, ColorType, (u32, u32)) {
	let img = image::jpeg::JpegDecoder::new(dfile).expect("Failed to load image file");
	let color_type = img.color_type();
	let total_bytes = img.total_bytes() as usize;
	// since this is a jpg, the data loaded in will always be RGB; the GPU will only accept RGBA so we need to zipper in an alpha channel
	let mut data: Vec<u8> = vec![0u8; total_bytes];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	(data, color_type, dimensions)
}

pub fn load_texture_ff(dfile: File) -> (Vec<u8>, ColorType, (u32, u32)) {
	let img = image::farbfeld::FarbfeldDecoder::new(dfile).expect("Failed to load image file");
	let color_type = img.color_type();
	let mut data: Vec<u8> = vec![0u8; img.total_bytes() as usize];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	(data, color_type, dimensions)
}

// pub fn convert_image_png_to_farbfeld<'a>(source_path: &'a str, dest_path: Option<&str>) {
// 	let mut stri: String;
// 	let dest_path = match dest_path {
// 		Some(dest) => dest,
// 		None => {
// 			// if no output specified, just take the source path, remove the .png, and append .ff
// 			stri = (source_path[..source_path.len()-4]).to_string();
// 			stri.push_str(".ff");
// 			&stri.as_str()
// 		}
// 	};
// 	let img_data = load_texture_png("test_normal.png");

// 	save_img_as_farbfeld(&img_data.0, dest_path, img_data.1);

// }


// pub fn save_img_as_farbfeld(data: &Vec<u8>, path: &str, dimensions: (u32, u32)) {
// 	let out = std::fs::File::create(path).expect("Failed to create output file");
// 	let encoder = image::farbfeld::FarbfeldEncoder::new(out);
// 	encoder.encode(data, dimensions.0, dimensions.1).expect("Failed to encode image");
// }

