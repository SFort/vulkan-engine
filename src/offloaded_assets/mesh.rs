
use std::{time::Instant, sync::{mpsc, Arc}, thread};
use vulkano::{buffer::{BufferUsage, CpuAccessibleBuffer}, device::Device};
use crate::{file_loading, common::Vertex};
use abstract_mesh::AbstractMesh;

pub mod abstract_mesh {
	use std::sync::Arc;
	use vulkano::{buffer::CpuAccessibleBuffer, device::Device};
	use crate::common::Vertex;

	pub trait AbstractMesh {
		fn new(path: String, dev: Arc<Device>) -> Self;
		fn get_data(&mut self) -> Option<(Arc<CpuAccessibleBuffer<[Vertex]>>, Arc<CpuAccessibleBuffer<[u32]>>)>;
	}
}

pub struct Mesh { 
	vertex_buffer: Option<Arc<CpuAccessibleBuffer<[Vertex]>>>,
	index_buffer: Option<Arc<CpuAccessibleBuffer<[u32]>>>,
	device: Arc<Device>,
	start: Instant,

	receiver: mpsc::Receiver<(Vec<Vertex>, Vec<u32>)>,
}

impl AbstractMesh for Mesh {
    fn new(path: String, dev: Arc<Device>) -> Self {
		let (sender, receiver) = mpsc::channel();

		let temp_path = path.clone();
		thread::spawn(move || {
			let mut data = Vec::new();
			let mut index_buffer = Vec::new();

			file_loading::load_ogl_model(&temp_path[..], &mut data, &mut index_buffer);

			sender.send((data, index_buffer)).expect("Failed to send texture data to main thread!");
		});

		return Mesh {
			vertex_buffer: None,
			index_buffer: None,
			device: dev,
			start: Instant::now(),
			receiver
		};
    }
    fn get_data(&mut self) -> Option<(Arc<CpuAccessibleBuffer<[Vertex]>>, Arc<CpuAccessibleBuffer<[u32]>>)> {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if self.vertex_buffer.is_none() {
			match self.receiver.try_recv() {
				Ok((vertex_data, index_data)) => {
					self.vertex_buffer = Some(CpuAccessibleBuffer::from_iter(self.device.clone(), BufferUsage::all(), false,
					vertex_data.into_iter()).expect("Failed to build vertex buffer"));
					self.index_buffer = Some(
						vulkano::buffer::cpu_access::CpuAccessibleBuffer::from_iter(
							self.device.clone(), 
							vulkano::buffer::BufferUsage::all(), 
							false, 
							index_data.into_iter()
					).expect("Failed to build index buffer"));
					println!("[Offloaded Model] Finished loading model; took {:.5} seconds", self.start.elapsed().as_secs_f64());
					return Some((self.vertex_buffer.as_ref().expect("Should not be possible! Value just assigned!").clone(),
							  self.index_buffer.as_ref().expect("Should not be possible! Value just assigned!").clone()));
				},
				Err(_) => {
					return None;
				} 
			}	
		} else {
			return Some((self.vertex_buffer.as_ref().expect("Should not be possible! `None` condition is already prepared for!").clone(),
					  self.index_buffer.as_ref().expect("Should not be possible! `None` condition is already prepared for!").clone()));
		}
    }
	
}

