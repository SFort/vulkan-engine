
use vulkano::{format::Format, image::{Dimensions, ImmutableImage}, device::Queue};
use std::{thread, sync::{mpsc, Arc}};
use crate::file_loading;

/// This struct stores and provides a texture that is loaded from the disk on a second thread. 
/// For textures it is better to provide an empty texture than return a conditionally valid resource imo, so we return `empty_texture`
/// when the code asks for the texture before it's ready
pub struct OffloadedTexture {
	empty_texture: Arc<ImmutableImage<Format>>,
	texture: Option<Arc<ImmutableImage<Format>>>,
	receiver: mpsc::Receiver<(Vec<u8>, (u32, u32), Format)>,
	queue: Arc<Queue>
}
impl OffloadedTexture {
	/// Constructs an OffloadedTexture that loads the image file at `path` and returns `empty_texture` if the image hasn't loaded yet
	pub fn new(path: String, queue: Arc<Queue>, empty_texture: Arc<ImmutableImage<Format>>, srgb: bool) -> OffloadedTexture {
		let (sender, receiver) = mpsc::channel();

		let ret = OffloadedTexture {
			texture: None,
			receiver,
			queue,
			empty_texture
		};
		thread::spawn(move || {
			sender.send(file_loading::load_texture(&path[..], srgb)).expect("Failed to send texture data to main thread!");
		});
		ret
	}

	/// Returns either the loaded texture or the empty texture if the loaded texture is not ready.
	pub fn get_texture<'b>(&'b mut self) -> Arc<ImmutableImage<Format>> {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.

		if self.texture.is_none() {
			match self.receiver.try_recv() {
				Ok((tex, dimensions, format)) => {
					// This immediately drops the future, so this function will block until the texture is ready
					self.texture = Some(
						ImmutableImage::from_iter(
							tex.iter().cloned(), 
							Dimensions::Dim2d {width: dimensions.0, height: dimensions.1},
							format,
							self.queue.clone()
						).expect("Failed to build vulkan texture").0);
					return self.texture.clone().expect("Should not be possible! Value just assigned!");
				},
				Err(_) => {
					return self.empty_texture.clone();
				} 
			}	
		} else {
			return self.texture.clone().expect("Should not be possible! `None` condition is already prepared for!");
		}
	}
}