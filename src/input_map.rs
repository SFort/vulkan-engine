use std::collections::HashMap;
use winit::event::VirtualKeyCode;

#[derive(Eq, PartialEq, Hash, Clone)]
pub enum KeyId {
	ScancodeId(u32),
	VirtualId(VirtualKeyCode)
}

pub fn scancodes_to_keyids(scancodes: Vec<u32>) -> Vec<KeyId> {
	let mut ret = Vec::with_capacity(scancodes.len());
	for &scancode in scancodes.iter() {
		ret.push(KeyId::ScancodeId(scancode));
	}
	ret
}

/// # InputMap
/// This represents an input map that stores the state of keys, represented either by scancode or `VirtualKeyCode`.
///
/// ## Actions
/// Actions are a helper concept, making it easier to connect game actions to key events.
/// Setup just requires passing in the name of the action and the set of keys that should trigger that action.
/// Once set up, simply checking the action name will get whether any of the set keys is pressed.
/// This makes changing key layouts much easier - instead of finding every use of `KEY_W` in your code, you can just
/// change the declaration of the `"walk_forward"` action for example, or even redefine it at any time in your code!
///
/// **Warning:** This structure does not implicitly convert between VirtualKeyCodes and scancodes; the physical key and virtual key `Q`, for example,
/// are not guaranteed to be the same!
pub struct InputMap {
	// Value is (pressed, just_changed)
	keys: HashMap<KeyId, (bool, bool)>,
	actions: HashMap<String, Vec<KeyId>>,
}

#[allow(dead_code)]
impl InputMap {
	/// Constructs a new, empty `InputMap`.
	pub fn new() -> Self {
		InputMap {
			keys: HashMap::new(),
			actions: HashMap::new()
		}
	}
	
	/// Helper function that checks if a scancode is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_scancode_pressed(&self, scancode: u32) -> bool {
		self.keys.get(&KeyId::ScancodeId(scancode)).unwrap_or(&(false, false)).0
	}
	
	/// Helper function that sets whether a scancode is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn set_scancode_pressed(&mut self, scancode: u32, pressed: bool) {
		self.keys.insert(KeyId::ScancodeId(scancode), (pressed, true));
	}

	/// Checks whether a key is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_pressed(&self, key: &KeyId) -> bool {
		self.keys.get(key).unwrap_or(&(false, false)).0
	}

	/// Checks whether a key was just pressed this frame.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_just_pressed(&self, key: &KeyId) -> bool {
		let (pressed, just_changed) = *self.keys.get(key).unwrap_or(&(false, false));
		pressed && just_changed
	}
	/// Checks whether a key was just released this frame.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_key_just_released(&self, key: &KeyId) -> bool {
		let (pressed, just_changed) = *self.keys.get(key).unwrap_or(&(false, false));
		(!pressed) && just_changed
	}

	/// Sets whether a key is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn set_key_pressed(&mut self, key: KeyId, pressed: bool) {
		self.keys.insert(key, (pressed, true));
	}

	/// Checks whether an action is pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_pressed(&self, action: String) -> bool {
		let opt_keys_for_action = self.actions.get(&action);
		if let Some(keys_for_action) = opt_keys_for_action {
			for key in keys_for_action.iter() {
				if self.is_key_pressed(key) {
					return true;
				}
			}
		}
		// if none of the keys were true, return false
		false
	}
	/// Checks whether an action was just pressed.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_just_pressed(&self, action: String) -> bool {
		let opt_keys_for_action = self.actions.get(&action);
		if let Some(keys_for_action) = opt_keys_for_action {
			for key in keys_for_action.iter() {
				if self.is_key_just_pressed(key) {
					return true;
				}
			}
		}
		// if none of the keys were true, return false
		false
	}
	/// Checks whether an action was just released.
	///
	/// **Warning:** There is no guarantee for scancodes and virtual keys for the same key to be the same; it is up to you to make sure they stay in sync!
	pub fn is_action_just_released(&self, action: String) -> bool {
		let opt_keys_for_action = self.actions.get(&action);
		if let Some(keys_for_action) = opt_keys_for_action {
			for key in keys_for_action.iter() {
				if self.is_key_just_released(key) {
					return true;
				}
			}
		}
		// if none of the keys were true, return false
		false
	}
	/// This is a helper function that implicitly converts `u32`s to `ScancodeId(u32)`s
	///
	/// This function will create a new action, or overwrite an old one with the same name.
	pub fn create_action_scancodes(&mut self, action: String, keys: Vec<u32>) {
		self.actions.insert(action, scancodes_to_keyids(keys));

	}/// This function will create a new action, or overwrite an old one with the same name.
	pub fn create_action(&mut self, action: String, keys: Vec<KeyId>) {
		self.actions.insert(action, keys);
	}
	/// Clears the `just_changed` field for all keys; call this at the end of a frame.
	pub fn clear_recently_changed(&mut self) {
		for (_, (_, recently_changed)) in self.keys.iter_mut() {
			*recently_changed = false;
		}
	}
}
// *if this works as I think it does* this removes the module unless the project is compiled in test mode
#[cfg(test)]
mod tests {
		use super::InputMap;
		use std::collections::HashMap;

		#[test]
	pub fn test_keys_input_map() {
		let mut input_map = InputMap::new();
		input_map.set_scancode_pressed(2, true);
		input_map.set_scancode_pressed(6, true);
		input_map.set_scancode_pressed(3, false);
		assert_eq!(input_map.is_scancode_pressed(2), true);
		assert_eq!(input_map.is_scancode_pressed(6), true);
		assert_eq!(input_map.is_scancode_pressed(3), false);
		assert_eq!(input_map.is_scancode_pressed(5), false);
	}

	#[test]
	pub fn test_actions_input_map() {
		let mut input_map = InputMap {
			keys: HashMap::new(),
			actions: HashMap::new()
		};
		input_map.set_scancode_pressed(2, true);
		input_map.set_scancode_pressed(6, true);
		input_map.set_scancode_pressed(3, false);

		input_map.create_action_scancodes(String::from("potato"), vec![2]);
		input_map.create_action_scancodes(String::from("aliens"), vec![8]);
		input_map.create_action_scancodes(String::from("evil"), vec![6, 3]);
		input_map.create_action_scancodes(String::from("dark"), vec![3, 6]);
		input_map.create_action_scancodes(String::from("santa"), vec![3]);

		assert_eq!(input_map.is_action_pressed("potato".into()), true);
		assert_eq!(input_map.is_action_pressed("aliens".into()), false);
		assert_eq!(input_map.is_action_pressed("evil".into()), true);
		assert_eq!(input_map.is_action_pressed("dark".into()), true);
		assert_eq!(input_map.is_action_pressed("santa".into()), false);
	}
	//TODO: Write test for VirtualKeyCodes

	#[test]
	fn bools_behind_pointers() {
		let mut test = false;
		let pointer = &mut test;

		*pointer = true;
		assert!(test);
	}
}
