pub fn is_power_of_two(x: u32) -> bool{
	(x != 0) && ((x&(x-1) == 0))
}