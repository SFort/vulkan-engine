
pub fn add_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] + b[0], a[1] + b[1], a[2] + b[2]]
}
pub fn add_vec3_scalar(a: [f32; 3], b: f32) -> [f32; 3] {
	[a[0] + b, a[1] + b, a[2] + b]
}

pub fn sub_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] - b[0], a[1] - b[1], a[2] - b[2]]
}
pub fn sub_vec3_scalar(a: [f32; 3], b: f32) -> [f32; 3] {
	[a[0] - b, a[1] - b, a[2] - b]
}

pub fn div_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] / b[0], a[1] / b[1], a[2] / b[2]]
}
pub fn div_vec3_scalar(a: [f32; 3], b: f32) -> [f32; 3] {
	[a[0] / b, a[1] / b, a[2] / b]
}

pub fn mul_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] * b[0], a[1] * b[1], a[2] * b[2]]
}
pub fn mul_vec3_scalar(a: [f32; 3], b: f32) -> [f32; 3] {
	[a[0] * b, a[1] * b, a[2] * b]
}
