use std::{cmp::Ordering, rc::Rc, sync::Arc};
use vulkano::{device::{Queue, Device}, framebuffer::{RenderPassAbstract, FramebufferAbstract}, format::{ClearValue, Format}, instance::{Instance, PhysicalDevice, PhysicalDeviceType::{DiscreteGpu, IntegratedGpu, VirtualGpu}}, swapchain::{CompositeAlpha, Capabilities, Surface, SwapchainCreationError, Swapchain, self}, sync::{self, GpuFuture}, image::{SwapchainImage, AttachmentImage}, command_buffer::{AutoCommandBufferBuilder, DynamicState, AutoCommandBuffer, pool::{standard::StandardCommandPoolAlloc}}};
use crate::{MIN_INNER_SIZE, offloaded_assets::mesh_instance::MeshInstance, common::{self, math_types::*}};
use winit::{window::{Icon, WindowBuilder, Window}, event_loop::EventLoop, dpi::{LogicalSize, Size}};
use vulkano_win::VkSurfaceBuild;
use sync::FlushError;
use cgmath::Deg;
use swapchain::{SwapchainAcquireFuture, AcquireError};
use shaders::vs;

mod utility;
use utility::*;
use common::math::is_power_of_two;

#[derive(Debug)]
pub enum UpdateError {
	UnsupportedValue
}

/// Represents a single window and all related structures and functions.
///
/// Essentially serves as an abstraction layer to make rendering easier
pub struct RenderServer<'a> {
	//physical device info
	pub physical: PhysicalDevice<'a>,
	pub device: Arc<Device>,
	pub capabilities: Capabilities,
	pub queue: Arc<Queue>,
	//window info
	pub surface: Arc<Surface<Window>>,
	pub dimensions: [u32; 2],
	pub aspect_ratio: f32,
	//low-level rendering info
	pub dynamic_state: DynamicState,
	pub render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
	pub swapchain: Arc<Swapchain<Window>>,
	pub images: Vec<Arc<SwapchainImage<Window>>>,
	pub framebuffers: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
	pub format: Format,
	//internal flags
	pub recreate_swapchain: bool,
	pub previous_frame_end: Option<Box<dyn GpuFuture>>,
	//mid-level rendering info
	msaa_level: u32,
	alpha_mode: CompositeAlpha,
	// high-level rendering info
	pub objects: Vec<Rc<dyn MeshInstance>>,
	pub lights: [vs::ty::light; 4],
	pub light_count: u32,
	pub projection_matrix: Matrix4,
	pub camera_position: Vector3,
	pub camera_rotation: Quaternion,
}

#[allow(dead_code)]
impl<'a> RenderServer<'a> {
	//TODO: Further break this apart into smaller reusable functions
   pub fn new(instance: &'a Arc<Instance>, events_loop: &EventLoop<()>) -> Self { 
		let (icon, icon_dimensions) = load_icon();
	
let physical: PhysicalDevice = PhysicalDevice::enumerate(&instance)
    .max_by(|&phy1, &phy2| match (phy1.ty(), phy2.ty()) {
	     (DiscreteGpu, _) => Ordering::Greater,
		 (_, DiscreteGpu) => Ordering::Less,
		 (IntegratedGpu, _) => Ordering::Greater,
		 (_, IntegratedGpu) => Ordering::Less,
		 (VirtualGpu, _) => Ordering::Greater,
		 (_, VirtualGpu) => Ordering::Less,
		 _ => Ordering::Equal,
	}).expect("Failed to get GPU");

		
		println!("Chose device '{}'", physical.name());
		
	
		let surface = WindowBuilder::new()
			.with_min_inner_size(MIN_INNER_SIZE)
			// We start at the minimum window size and then resize to our normal size later
			// to prevent bugs with certain overlay tools
			.with_inner_size(MIN_INNER_SIZE)
			.with_title("Vulkan Experiment")
			.with_window_icon(Some(
				Icon::from_rgba(icon, icon_dimensions.0, icon_dimensions.1).expect("Failed to build icon")
			))
			.build_vk_surface(events_loop, instance.clone()).expect("Failed to build surface");
	
		let capabilities = surface.capabilities(physical)
			.expect("failed to get surface capabilities");
	
		let (queue, device) = get_device_queue(physical);
		let dimensions = surface.window().inner_size().into();
		let msaa_level = get_max_supported_msaa_level(physical);
		let format = capabilities.supported_formats[0].0;
		let alpha_mode = get_supported_alpha(capabilities.supported_composite_alpha);
		let mut dynamic_state = DynamicState {
			line_width: None,
			viewports: None,
			scissors: None,
			compare_mask: None,
			write_mask: None,
			reference: None,
		};
		let (swapchain, images) = build_swapchain(
			device.clone(), 
			surface.clone(), 
			&capabilities, 
			format, 
			dimensions, 
			queue.clone(), 
			get_supported_alpha(capabilities.supported_composite_alpha)
		);
		let render_pass = build_render_pass(msaa_level, device.clone(), format);
		let intermediary = AttachmentImage::transient_multisampled(
			device.clone(),
			dimensions,
			msaa_level,
			format,
		).unwrap();
		let framebuffers = window_size_dependent_setup(
			&images,
			intermediary.clone(),
			render_pass.clone(),
			device.clone(),
			msaa_level,
			&mut dynamic_state,
		);

		let aspect_ratio =  dimensions[0] as f32 / dimensions[1] as f32;

		let ret = Self {
			//physical device/window info
			physical, 
			device: device.clone(), 
			capabilities,
			queue: queue.clone(),
			//window info
			surface: surface.clone(),
			dimensions,
			aspect_ratio,
			//low-level rendering info
			dynamic_state,
			render_pass, // From here down we use temp values to be replaced by associated functions
			swapchain,
			images,
			framebuffers,
			format,
			//internal flags
			recreate_swapchain: true, 
			previous_frame_end: Some(sync::now(device.clone()).boxed()),
			//mid-level rendering info
			msaa_level,
			alpha_mode,
			//high-level rendering data
			objects: Vec::new(),
			lights: [vs::ty::light::default(); 4],
			light_count: 0u32,
			projection_matrix: common::get_projection_matrix(Deg(90.0).into(), aspect_ratio, 0.01, 100.0),
			camera_position: Vector3::new(0.0, 0.0, 5.0),
			camera_rotation: Quaternion::new(1.0, 0.0, 0.0, 0.0)
		};	

		println!("Max sample count: {}", ret.msaa_level);
		println!("Supported formats: {:?}", ret.capabilities.supported_formats);
		ret
	}

	/// Rebuilds the render pass for the struct using the stored settings.
	pub fn build_render_pass(&mut self) {		
		self.render_pass = build_render_pass(self.msaa_level, self.device.clone(), self.format.clone());
		for obj in self.objects.iter_mut() {
			Rc::get_mut(obj).expect("Failed to get reference to obj").rebuild_pipeline(self.render_pass.clone());
		}
	}

	pub fn add_light(&mut self, light: vs::ty::light) -> Result<(), UpdateError>{
		if self.light_count >= 4 {
			Err(UpdateError::UnsupportedValue)
		} else {
			self.lights[self.light_count as usize] = light;
			self.light_count += 1;
			Ok(())
		}
	}

	pub fn build_swapchain(&mut self) {
		let (swapchain, images) = 
			build_swapchain(
				self.device.clone(), 
				self.surface.clone(), 
				&self.capabilities, 
				self.format.clone(), 
				self.dimensions, 
				self.queue.clone(), 
				self.alpha_mode
			);
		self.swapchain = swapchain;
		self.images = images;
	}
	pub fn rebuild_swapchain(&mut self) {
		// Get the new dimensions of the window.
		self.dimensions = self.surface.window().inner_size().into();
		let (new_swapchain, new_images) = {
			match self.swapchain.recreate_with_dimensions(self.dimensions) {
				Ok(r) => r,
				// This error tends to happen when the user is manually resizing the window.
				// Simply restarting the loop is the easiest way to fix this issue.
				Err(SwapchainCreationError::UnsupportedDimensions) => return,
				Err(e) => panic!("Failed to recreate swapchain: {:?}", e),
			}
		};
		self.swapchain = new_swapchain;
		let intermediary = AttachmentImage::transient_multisampled(
			self.device.clone(),
			self.dimensions,
			self.msaa_level,
			self.format,
		).unwrap();

		// Because framebuffers contains an Arc on the old swapchain, we need to
		// recreate framebuffers as well.
		self.framebuffers = window_size_dependent_setup(
			&new_images,
			intermediary.clone(),
			self.render_pass.clone(),
			self.device.clone(),
			self.msaa_level,
			&mut self.dynamic_state,
		);
		self.recreate_swapchain = false;
	}

	pub fn get_max_supported_msaa_level(&self) -> u32 {
		get_max_supported_msaa_level(self.physical.clone())
	}

	pub fn supports_msaa_level(&self, msaa_level: u32) -> bool {
		let supported_levels = 
			self.physical.limits().framebuffer_color_sample_counts() & self.physical.limits().framebuffer_depth_sample_counts();
		is_power_of_two(msaa_level) && (supported_levels & msaa_level) > 0
	}

	pub fn set_msaa_level(&mut self, msaa_level: u32) -> Result<(), UpdateError> {
		if self.supports_msaa_level(msaa_level) {
			self.msaa_level = msaa_level;
			self.build_render_pass();
			self.trigger_recreate_swapchain();
			Ok(())
		} else {
			println!("Tried to update msaa level to {} from {}; max supported is {}", msaa_level, self.msaa_level, get_max_supported_msaa_level(self.physical.clone()));
			Err(UpdateError::UnsupportedValue)
		}
	}

	pub fn get_msaa_level(&self) -> u32 {
		self.msaa_level
	}

	fn get_queue(&self) -> (Arc<Queue>, Arc<Device>) {
		get_device_queue(self.physical)
	}

	pub fn get_default_alpha_mode(&self) -> CompositeAlpha {
		let supported_alpha = self.capabilities.supported_composite_alpha; //.iter().next().expect("Failed to get valid alpha mode")
		get_supported_alpha(supported_alpha)
	}
	
	pub fn get_alpha_mode(&self) -> CompositeAlpha {
		self.alpha_mode
	}

	pub fn set_alpha_mode(&mut self, alpha_mode: CompositeAlpha) -> Result<(), UpdateError> {
		let supported_alpha = self.capabilities.supported_composite_alpha;
		if supported_alpha.supports(alpha_mode) {
			self.alpha_mode = alpha_mode;
			self.build_render_pass();
			self.trigger_recreate_swapchain();
			Result::Ok(())
		} else {
			Result::Err(UpdateError::UnsupportedValue)
		}
	}

	pub fn add_object(&mut self, object: Rc<dyn MeshInstance>) {
		self.objects.push(object);
	}

	pub fn add_objects(&mut self, mut objects: Vec<Rc<dyn MeshInstance>>) {
		self.objects.append(&mut objects);
	}

	pub fn trigger_recreate_swapchain(&mut self) {
		self.recreate_swapchain = true;
	}

	pub fn update_projection_matrix(&mut self) {// when the window resizes, we need to rebuild the projection matrix to match the new aspect ratio
		let new_aspect_ratio = self.dimensions[0] as f32 / self.dimensions[1] as f32;
		if self.aspect_ratio != new_aspect_ratio {
			self.aspect_ratio = new_aspect_ratio;
			self.projection_matrix = common::get_projection_matrix(Deg(90.0).into(), self.aspect_ratio, 0.01, 100.0);
		}
	}

	pub fn set_window_size(&mut self, width: f64, height: f64) {
		self.surface.window().set_inner_size(Size::Logical(LogicalSize::new(width, height)));
	}

	pub fn get_clear_data(&self) -> Vec<ClearValue> {
		if self.msaa_level > 1 {
			vec![[0.0, 0.0, 0.0, 1.0].into(), ClearValue::None, ClearValue::Depth(1.0)]
		} else {
			vec![[0.0, 0.0, 0.0, 1.0].into(), ClearValue::Depth(1.0)]
		}
	}

	pub fn get_view_matrix(&self) -> Matrix4 {
		Matrix4::from_translation(-self.camera_position) * Matrix4::from(-self.camera_rotation)
	}

	/// Polls for changes to objects on the physics thread through the mpsc pipe.
	pub fn poll_object_changes() {
		todo!();
	}

	pub fn draw(&mut self, delta: f64) {
		// It is important to call this function from time to time, otherwise resources will keep
		// accumulating and you will eventually reach an out of memory error.
		// Calling this function polls various fences in order to determine what the GPU has
		// already processed, and frees the resources that are no longer needed.
		self.previous_frame_end.as_mut().expect("Failed to get previous frame end").cleanup_finished();

		// Whenever the window resizes we need to recreate everything dependent on the window size
		if self.recreate_swapchain {
			self.rebuild_swapchain();
			self.recreate_swapchain = false;
		}
		self.update_projection_matrix();

		// Gets the next available image or triggers a swapchain rebuild as necessary
		let (image_num, suboptimal, acquire_future) =
			match swapchain::acquire_next_image(self.swapchain.clone(), None) {
				Ok(r) => r,
				Err(AcquireError::OutOfDate) => {
					self.recreate_swapchain = true;
					return;
				}
				Err(e) => panic!("Failed to acquire next image: {:?}", e),
			};
		// acquire_next_image can be successful, but suboptimal. This means that the swapchain image
		// will still work, but it may not display correctly. With some drivers this can be when
		// the window resizes, but it may not cause the swapchain to become out of date.
		if suboptimal {
			self.recreate_swapchain = true;
		}
				
		// Apparantly `Depth(f32)` is not the same as an f32, for... reasons
		let clear_data = self.get_clear_data();
		let mut builder = AutoCommandBufferBuilder::primary_one_time_submit(
			self.device.clone(), 
			self.queue.family()
		).expect("Failed to build command buffer builder");
		builder
			.begin_render_pass(self.framebuffers[image_num].clone(), false, clear_data)
			.expect("Failed to begin render pass");

		let view_matrix = self.get_view_matrix();
		// render all models
		for obj in self.objects.iter_mut() {
			Rc::get_mut(obj).expect("Failed to get mutable reference to object; other handle exists?")
				.draw(
					&mut builder, 
					self.dynamic_state.clone(), 
					&self.projection_matrix, 
					&view_matrix, 
					&self.lights, 
					self.light_count,
					delta as f32
				);
		}
		builder
			.end_render_pass()
			.expect("Failed to end render pass");
		let command_buffer = builder.build().expect("Failed to build command buffer");
		self.update_future(acquire_future, command_buffer, image_num);
	}
	
	fn update_future(
		&mut self, 
		acquire_future: SwapchainAcquireFuture<Window>,
		command_buffer: AutoCommandBuffer<StandardCommandPoolAlloc>,
		image_num: usize
	) {
		let future = self.previous_frame_end
			.take()
			.expect("Failed to take future")
			.join(acquire_future)
			.then_execute(self.queue.clone(), command_buffer)
			.expect("Failed to add future execution")
			.then_swapchain_present(self.queue.clone(), self.swapchain.clone(), image_num)
			.then_signal_fence_and_flush();

		match future {
			Ok(future) => {
				self.previous_frame_end = Some(future.boxed());
			}
			Err(FlushError::OutOfDate) => {
				self.recreate_swapchain = true;
				self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
			}
			Err(e) => {
				println!("Failed to flush future: {:?}", e);
				self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
			}
		}
	}

}
