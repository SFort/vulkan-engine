use std::sync::Arc;
use vulkano::{device::{Queue, Device}, framebuffer::{Framebuffer, RenderPassAbstract, FramebufferAbstract}, format::Format, instance::PhysicalDevice, swapchain::{CompositeAlpha, Capabilities, Surface, Swapchain, SurfaceTransform, PresentMode, FullscreenExclusive, ColorSpace, SupportedCompositeAlpha}, image::{SwapchainImage, AttachmentImage, ImageUsage}, pipeline::viewport::Viewport, command_buffer::{DynamicState}};
use crate::file_loading;
use winit::window::Window;

/// This method is called once during initialization, then again whenever the window is resized
pub fn window_size_dependent_setup(
	images: &[Arc<SwapchainImage<Window>>],
	intermediary: Arc<AttachmentImage>,
	render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
	device: Arc<Device>,
	msaa_level: u32,
	dynamic_state: &mut DynamicState,
) -> Vec<Arc<dyn FramebufferAbstract + Send + Sync>> {
	let dimensions = images[0].dimensions();

	let viewport = Viewport {
		origin: [0.0, 0.0],
		dimensions: [dimensions[0] as f32, dimensions[1] as f32],
		depth_range: 0.0..1.0,
	};
	dynamic_state.viewports = Some(vec![viewport]);

	let dimensions = images[0].dimensions();

	let depth = vulkano::image::attachment::AttachmentImage::transient_multisampled(
		device.clone(),
		dimensions, 
		msaa_level,
		vulkano::format::D16Unorm
	).unwrap();	  

	images
		.iter()
		.map(|image| {
			if msaa_level > 1 {
				Arc::new(
					Framebuffer::start(render_pass.clone())
						.add(intermediary.clone()).expect("Failed to add inter to framebuffer")
						.add(image.clone())       .expect("Failed to add image to framebuffer")
						.add(depth.clone())       .expect("Failed to add depth to framebuffer")
						.build()                              .expect("Failed to build framebuffer"),
				) as Arc<dyn FramebufferAbstract + Send + Sync>
			} else {
				Arc::new(
					Framebuffer::start(render_pass.clone())
						.add(image.clone())       .expect("Failed to add image to framebuffer")
						.add(depth.clone())       .expect("Failed to add depth to framebuffer")
						.build()                              .expect("Failed to build framebuffer"),
				) as Arc<dyn FramebufferAbstract + Send + Sync>
			}
		})
		.collect::<Vec<_>>()
}

pub fn load_icon() -> (Vec<u8>, (u32, u32)) {
	let (icon, icon_dimensions, icon_fmt) = file_loading::load_texture("icon.png", true);
	//TODO: Convert from other formats to R8G8B8A8 instead of crashing
	assert_eq!(icon_fmt, Format::R8G8B8A8Srgb);
	(icon, icon_dimensions)
}
pub fn get_device_queue(physical: PhysicalDevice) ->  (Arc<Queue>, Arc<Device>) {
	let queue_family = physical.queue_families()
	.find(|&q| q.supports_graphics() && q.supports_compute())
	.expect("couldn't find a graphical queue family");

	let (device, mut queues) = {
		let device_ext = vulkano::device::DeviceExtensions {
			khr_swapchain: true,
			.. vulkano::device::DeviceExtensions::none()
		};
		// println!("{:?}", physical.supported_features());
		Device::new(physical, physical.supported_features(), &device_ext,
			[(queue_family, 0.5)].iter().cloned()).expect("failed to create device")
	};

	let queue = queues.next().expect("Failed to get queue");
	(queue, device)
}
pub fn build_swapchain(
	device: Arc<Device>, 
	surface: Arc<Surface<Window>>, 
	caps: &Capabilities, 
	format: Format, 
	dimensions: [u32; 2],
	queue: Arc<Queue>,
	alpha: CompositeAlpha,
) -> (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>) {
	Swapchain::new(
		device.clone(), 
		surface.clone(),
		caps.min_image_count, 
		format, 
		dimensions, 
		1, 
		ImageUsage::color_attachment(), 
		&queue,
		SurfaceTransform::Identity, 
		alpha, 
		PresentMode::Mailbox, 
		FullscreenExclusive::Default,
		true, 
		ColorSpace::SrgbNonLinear
	).expect("failed to create swapchain")
}

pub fn get_supported_alpha(supported_alpha: SupportedCompositeAlpha) -> CompositeAlpha {
	if      supported_alpha.supports(CompositeAlpha::Inherit) {CompositeAlpha::Inherit}
	else if supported_alpha.supports(CompositeAlpha::Opaque) {CompositeAlpha::Opaque}
	else if supported_alpha.supports(CompositeAlpha::PostMultiplied) {CompositeAlpha::PostMultiplied}
	else if supported_alpha.supports(CompositeAlpha::PreMultiplied) {CompositeAlpha::PreMultiplied}
	else {panic!("Failed to find valid alpha mode")}
}

pub fn build_render_pass(msaa_level: u32, device: Arc<Device>, format: Format) -> Arc<dyn RenderPassAbstract + Send + Sync> {
	{
		if msaa_level > 1 {
			Arc::new(
				vulkano::single_pass_renderpass!(
					device.clone(),
					attachments: {
						intermediary: {
							load: Clear,
							store: DontCare,
							format: format,
							samples: msaa_level,     // This has to match the image definition.
						},
						color: {
							load: DontCare,
							store: Store,
							format: format,
							samples: 1,
						},
						depth: {
							load: Clear,
							store: DontCare,
							format: Format::D16Unorm,
							samples: msaa_level,
						}
					},
					pass: {
						color: [intermediary],
						depth_stencil: {depth},
						resolve: [color],
					}
				).expect("Failed to build render pass")
			)
		} else {
			Arc::new(
				vulkano::single_pass_renderpass!(
					device.clone(),
					attachments: {
						color: {
							load: Clear,
							store: Store,
							format: format,
							samples: msaa_level,
						},
						depth: {
							load: Clear,
							store: DontCare,
							format: Format::D16Unorm,
							samples: msaa_level,
						}
					},
					pass: {
						color: [color],
						depth_stencil: {depth},
						resolve: [],
					}
				).expect("Failed to build render pass")
			)
		}
	}
}

pub fn get_max_supported_msaa_level(physical: PhysicalDevice) -> u32 {
	// this gets only the levels of MSAA supported by both the color and depth buffer
	let supported_levels = physical.limits().framebuffer_color_sample_counts() & physical.limits().framebuffer_depth_sample_counts();
	if supported_levels & 64 > 0 {
		64
	} else if supported_levels & 32 > 0 {
		32
	} else if supported_levels & 16 > 0 {
		16
	} else if supported_levels & 8 > 0 {
		8
	} else if supported_levels & 4 > 0 {
		4
	} else if supported_levels & 2 > 0 {
		2
	} else { //TODO: This will crash; Vulkan apparantly does not like using the multisampling pipeline without multisampling
		1
	}
}