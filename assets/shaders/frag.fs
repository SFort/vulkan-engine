#version 450
#define MAX_LIGHTS 4

layout(location = 0) out vec4 color;

layout(location = 0) in  vec2 uv;
layout(location = 1) in  vec3 model_normal;
layout(location = 2) in  vec3 eye_dir_tanspace;
layout(location = 3) in  vec3 normal_tanspace;

layout(location = 4) flat in  uint light_count;
layout(location = 5) in  vec3[MAX_LIGHTS] lights_direction;
layout(location = 5 + MAX_LIGHTS)  
                     in  vec3[MAX_LIGHTS] lights_color;
layout(location = 5 + (MAX_LIGHTS * 2)) 
                     in  float[MAX_LIGHTS] lights_strength;

layout(binding = 2) uniform sampler2D albedo_texture;
layout(binding = 3) uniform sampler2D normal_texture;



void main() {
	vec3 normal = normalize(texture(normal_texture, uv).rgb * 2.0 - 1.0);
	normal = normal / 2.0 + vec3(0.0, 0.5, 0.0);
	vec3 albedo = texture(albedo_texture, uv).rgb;

	vec3 net_color = vec3(0.0, 0.0, 0.0);
	// ambient light
	net_color.rgb = net_color.rgb + (albedo * 0.2);

	// making sure we don't overrun our buffer
	uint loop_max = min(MAX_LIGHTS, light_count);

	for (uint i = 0; i < loop_max; i++) {
		vec3  light_vec = lights_direction[i];
		float light_distance = length(light_vec);
		vec3  light_dir = normalize(light_vec);

		float cosTheta = clamp(dot(normal, light_dir), 0,1);
		net_color.rgb += (1.0 / pow(light_distance, 2.0)) * lights_strength[i] * cosTheta * lights_color[i] * albedo;
	}
	color = vec4(net_color, 1.0);
	// color.rgb = model_normal;
}