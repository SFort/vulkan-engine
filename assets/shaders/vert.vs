#version 450
#define MAX_LIGHTS 4

struct light {
	vec3 position;
	vec3 color;
	float strength;
};

layout(location = 0) in  vec3 position;
layout(location = 1) in  vec2 uv;
layout(location = 2) in  vec3 normal;
layout(location = 3) in  vec3 tangent;
layout(location = 4) in  vec3 bitangent;

layout(binding = 0) uniform matrices {
	mat4 model;
	mat4 view;
	mat4 projection;
	mat4 mvp;
	mat3 mv3x3_matrix;
};
layout(binding = 1) uniform lighting {
	uint light_count;
	light[MAX_LIGHTS] lights;
};



layout(location = 0) out vec2 f_uv;
layout(location = 1) out vec3 f_model_normal;
layout(location = 2) out vec3 eye_dir_tanspace;
layout(location = 3) out vec3 f_normal_tanspace;

layout(location = 4) flat out uint f_light_count;
layout(location = 5) out vec3[MAX_LIGHTS] f_lights_direction;
layout(location = 5 + MAX_LIGHTS)  
                     out vec3[MAX_LIGHTS] f_lights_color;
layout(location = 5 + (MAX_LIGHTS * 2)) 
                     out float[MAX_LIGHTS] f_lights_strength;

void main() {
	vec3 normal_camspace = (view * model * vec4(normalize(normal), 0.0)).xyz;
	vec3 tangent_camspace = (view * model * vec4(normalize(tangent), 0.0)).xyz;
	vec3 bitangent_camspace = (view * model * vec4(normalize(bitangent), 0.0)).xyz;

	mat3 TBN = transpose(mat3(
		tangent_camspace,
		bitangent_camspace,
		normal_camspace
	));

	vec3 vertex_pos_camspace = (view * model * vec4(position, 1)).xyz;

	vec3 eye_dir_camspace = -vertex_pos_camspace;
	eye_dir_tanspace = eye_dir_camspace;

	f_light_count = light_count;

	uint loop_max = min(MAX_LIGHTS, light_count);

	for (uint i = 0; i < loop_max; i++) {
		vec3 l_position = (view * vec4(lights[i].position, 1.0)).xyz;
		vec3 light_dir_camspace = l_position + eye_dir_camspace;
		// temp workaround because Vulkan gets crashy when writing to these arrays
		float l_strength = lights[i].strength + 0.00000;
		vec3 l_color = lights[i].color + vec3(0.00000);
		f_lights_direction[i] = TBN * light_dir_camspace;
		f_lights_color[i] = l_color;
		f_lights_strength[i] = l_strength;
	}
	// for (uint i = light_count; i < MAX_LIGHTS; i++) {
	// 	f_lights_direction[i] = vec3(0.0);
	// 	f_lights_color[i] = vec3(0.0);
	// 	f_lights_strength[i] = 0.0;
	// }
		
	f_model_normal  = normalize(tangent_camspace);
	
	f_uv = uv;
	gl_Position = mvp * vec4(position, 1);
}