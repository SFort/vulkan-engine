vulkano_shaders::shader! {
	ty: "vertex",
	path: "../assets/shaders/vert.vs"
}

impl ty::light {
	pub fn create_empty_buffer() -> [ty::light; 4] {
		[ty::light::default(); 4]
	}
	
}

impl Default for ty::light {
	fn default() -> Self {
		ty::light {
			_dummy0: [0u8; 4],
			color: [0.0f32; 3],
			position: [0.0f32; 3],
			strength: 0.0f32
		}
	}
}