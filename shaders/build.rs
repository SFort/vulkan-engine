
use std::{io::Write, fs::File};

fn main() {
	println!("cargo:rerun-if-changed=../assets/shaders/frag.fs");
	println!("cargo:rerun-if-changed=../assets/shaders/vert.vs");
	let src_handle = File::open("src/lib.rs").expect("Failed to get metadata handle");
	println!("{:?}", src_handle.metadata());
	let source = std::fs::read_to_string("src/lib.rs").expect("Failed to read lib.rs");
	println!("{}", source);
	let mut dest = File::create("src/lib.rs").expect("Failed to create write handle for lib.rs");
	dest.write_all(source.as_bytes()).expect("Failed to write to lib.rs");
	println!("{:?}", dest.metadata());
}